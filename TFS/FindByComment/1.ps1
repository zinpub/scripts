﻿$LastEncoding = [Console]::OutputEncoding 
[Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding(1251) 

$CSList = tf history .\ -r  -format:detailed | Out-String

$sep = "-------------------------------------------------------------------------------"
$FindStr = '*' + $args[0] + '*' 
#$FindStr = '*141*'
$FileN = $args[1]
#$FileN = 'aaa.txt'


$CSArr = $CSList -split $sep

$Res = New-Object System.Collections.ArrayList

foreach ($CS in $CSArr)
{
  if ($CS -like $FindStr)
  {
    $Res.Add($CS)
    #$Res = $Res + $sep + $CS + "`n"
    #Write-Host $sep
    #Write-Host $CS
  }
} 

$Ttl =  'Found ChangeSets for ' + $FindStr

if ($FileN -ne $null) 
{
  $Res | Out-File $FileN -Encoding utf8 
}
else
{
  $Res | Out-GridView -PassThru -Title $Ttl
}

[Console]::OutputEncoding = $LastEncoding