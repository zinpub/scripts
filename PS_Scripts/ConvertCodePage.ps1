﻿
$Fls = Get-Content -LiteralPath $Args[0]

foreach ($F in $Fls) 
{
  $CMD = "/F:" + $Args[1] + " " + "/T:" + $Args[2]
  
  cmd /c "C:\Util\cpcnv\cpcnv.exe /S:$F $CMD"


    Try  
    {
        [System.IO.FileInfo] $file = Get-Item -Path $F
        $sequenceBOM = New-Object System.Byte[] 3
        $reader = $file.OpenRead()
        $bytesRead = $reader.Read($sequenceBOM, 0, 3)
        $reader.Dispose()
        #A UTF-8+BOM string will start with the three following bytes. Hex: 0xEF0xBB0xBF, Decimal: 239 187 191
        if ($bytesRead -eq 3 -and $sequenceBOM[0] -eq 239 -and $sequenceBOM[1] -eq 187 -and $sequenceBOM[2] -eq 191)
        {
            $utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False)
            [System.IO.File]::WriteAllLines($F, (Get-Content $F), $utf8NoBomEncoding)
            Write-Host "Remove UTF-8 BOM successfully : $F"
        }
        Else
        {
            Write-Warning "Not UTF-8 BOM file : $F"
        }    
    }
    Catch [Exception] 
    { 
        Write-Error $_.Exception.ToString() 
    }  
}