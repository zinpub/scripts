@echo off
setlocal enabledelayedexpansion

for /F "tokens=1-3 delims=:." %%a in ("%TIME%") do set "current_time=%%a:%%b"

echo "%DATE% %current_time%" | clip