﻿#git log --pretty=format:"[%%h - %%an - %%cd] %%s" --grep %1 --regexp-ignore-case --name-only -%2
#git log --format="%cd't%h't%an't%s" --grep "затесалась" --regexp-ignore-case -10

$LastEncoding = [Console]::OutputEncoding 
[Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding(65001) 

$FindMe = $args[0]
$CSLimit = $args[1]
$FileToSave = $args[2]

$FoundCS = (git log --date=format:"%d.%m.%y %H:%M:%S" --format="%cd`t%h`t%an`t%s" --grep $FindMe --regexp-ignore-case -$CSLimit | ConvertFrom-Csv -Delimiter "`t" -Header ("DT","hash","Author","Comment"))

if (($FileToSave -ne $null) -and ($FileToSave.Trim() -ne ""))
{
  $Res = New-Object System.Collections.ArrayList

  foreach ($CS in $FoundCS)
  {
    $Res.Add($CS.DT + " " + " " + $CS.Author.PadRight(30) + " " + $CS.hash + "`n" + $CS.Comment)
  } 

  $Res | Out-File $FileToSave -Encoding utf8
}
else
{
  $Ttl = "Поиск '" + $FindMe + "'"
  $FoundCS | Out-GridView -PassThru -Title $Ttl
}

[Console]::OutputEncoding = $LastEncoding